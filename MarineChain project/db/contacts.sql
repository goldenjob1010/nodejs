/*
Navicat PGSQL Data Transfer

Source Server         : localhost1
Source Server Version : 90606
Source Host           : localhost:5432
Source Database       : bustabitdb
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90606
File Encoding         : 65001

Date: 2018-04-04 23:17:31
*/


-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS "public"."contacts";
CREATE TABLE "public"."contacts" (
"id" int4 NOT NULL,
"customer_user" varchar(255) COLLATE "default",
"customer_email" varchar(255) COLLATE "default",
"customer_message" varchar(255) COLLATE "default",
"customer_pub_date" varchar COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO "public"."contacts" VALUES ('1', 'dsa', 'dsasd@gmai.comf', 'fsafa', '2018-04-04T01:19:46.110+08:00');
INSERT INTO "public"."contacts" VALUES ('2', 'sdad', 'asddsa@g.com', 'das', '2018-04-04T01:23:52.982+08:00');
INSERT INTO "public"."contacts" VALUES ('3', 'steve', 'steve@innoshore.com', 'this is test', '2018-04-04T01:32:30.865+08:00');
INSERT INTO "public"."contacts" VALUES ('4', 'fsd', 'fsa@gmail.com', 'dfsa', '2018-04-04T01:39:01.926+08:00');
INSERT INTO "public"."contacts" VALUES ('5', 'Max', 'happwork1010@gmail.com', 'great content', '2018-04-04T11:11:44.189+08:00');
INSERT INTO "public"."contacts" VALUES ('6', 'fds', 'fsda@gmil.com', 'fsa', '2018-04-04T11:33:45.681+08:00');
INSERT INTO "public"."contacts" VALUES ('7', '123', '123@test.com', '1231231242341324134123', '2018-04-04T16:07:08.128+08:00');
INSERT INTO "public"."contacts" VALUES ('8', 'steve', 'steve@innoshore.com', '1231231231231231', '2018-04-04T16:07:47.402+08:00');
INSERT INTO "public"."contacts" VALUES ('9', 'fsdf', 'testtest@gmail.com', 'great test form', '2018-04-04T18:50:17.848+08:00');
INSERT INTO "public"."contacts" VALUES ('10', 'qwer', 'qwer@test.com', 'qwerqqwer', '2018-04-04T21:59:16.075+08:00');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table contacts
-- ----------------------------
ALTER TABLE "public"."contacts" ADD PRIMARY KEY ("id");
