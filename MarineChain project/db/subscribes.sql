/*
Navicat PGSQL Data Transfer

Source Server         : localhost1
Source Server Version : 90606
Source Host           : localhost:5432
Source Database       : bustabitdb
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90606
File Encoding         : 65001

Date: 2018-04-04 23:17:50
*/


-- ----------------------------
-- Table structure for subscribes
-- ----------------------------
DROP TABLE IF EXISTS "public"."subscribes";
CREATE TABLE "public"."subscribes" (
"id" int8,
"email_item" text COLLATE "default",
"arrival_date" text COLLATE "default",
"subscribe_status" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of subscribes
-- ----------------------------
INSERT INTO "public"."subscribes" VALUES ('1', 'oskar@gmail.com', '23-03-2018', 'Unconfirmed');
INSERT INTO "public"."subscribes" VALUES ('2', 'oskar111@gmail.com', '23-03-2018', 'Unconfirmed');
INSERT INTO "public"."subscribes" VALUES ('3', 'oskar12321312@gmail.com', '23-03-2018', 'Unconfirmed');
INSERT INTO "public"."subscribes" VALUES ('4', 'helloworld@gmail.com', '23-03-2018', 'Unconfirmed');
INSERT INTO "public"."subscribes" VALUES ('5', 'test@tt.com', '24-03-2018', 'Unconfirmed');
INSERT INTO "public"."subscribes" VALUES ('6', 'test@tt.com', '24-03-2018', 'Unconfirmed');
INSERT INTO "public"."subscribes" VALUES ('7', 'test1001@gmail.com', '25-03-2018', 'unconfirmed');
INSERT INTO "public"."subscribes" VALUES ('8', 'max1001@gmail.com', '2018-03-25T10:31:29.256+02:00', 'unconfirmed');
INSERT INTO "public"."subscribes" VALUES ('9', 'frosttsalem@gmail.com', '2018-03-29T04:49:50.067+08:00', 'unconfirmed');
INSERT INTO "public"."subscribes" VALUES ('10', '', '2018-04-04T05:43:28.573+08:00', 'unconfirmed');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
