/*
Navicat PGSQL Data Transfer

Source Server         : localhost1
Source Server Version : 90606
Source Host           : localhost:5432
Source Database       : bustabitdb
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90606
File Encoding         : 65001

Date: 2018-04-04 23:17:42
*/


-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS "public"."posts";
CREATE TABLE "public"."posts" (
"id" int8 NOT NULL,
"news_date" text COLLATE "default",
"news_content" text COLLATE "default",
"news_title" text COLLATE "default",
"news_img1" varchar(500) COLLATE "default",
"news_img2" varchar(500) COLLATE "default",
"news_img3" varchar(500) COLLATE "default",
"news_img4" varchar(500) COLLATE "default",
"user_id" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO "public"."posts" VALUES ('2', '27-03-2018', 'content', 'test-test', 'images (5).jpg', 'download (1).jpg', 'images (8).jpg', 'images (9).jpg', '1');
INSERT INTO "public"."posts" VALUES ('3', '28-03-2018', 'content', 'our news', 'download (1).jpg', 'images (4).jpg', 'images (4).jpg', 'images (4).jpg', '1');
INSERT INTO "public"."posts" VALUES ('4', '28-03-2018', 'content', 'new1', 'images (1).jpg', 'images (9).jpg', 'images (8).jpg', 'images (6).jpg', '1');
INSERT INTO "public"."posts" VALUES ('5', '28-03-2018', 'content', 'test3', 'images (4).jpg', 'images (8).jpg', 'images (7).jpg', 'images (3).jpg', '1');
INSERT INTO "public"."posts" VALUES ('6', '28-03-2018', 'content', 'test43', 'images (5).jpg', 'images (8).jpg', 'images (8).jpg', 'images (3).jpg', '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
